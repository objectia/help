module.exports = {
    title: 'Objectia Help Center', 
    description: 'Objectia Help Center',
    base: '/',
    port: 3004,
    dest: 'public',
    plugins: [
        '@vuepress/nprogress'
    ],
    head: [
        ['link', { rel: 'apple-touch-icon', sizes: '180x180', type: "image/png", href: '/favicons/apple-icon-180x180.png' }],
        ['link', { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicons/favicon-32x32.png' }],
        ['link', { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicons/favicon-16x16.png' }],
        ['link', { rel: 'manifest', href: '/favicons/manifest.json' }],
        ['link', { rel: 'shortcut icon', type: "image/x-icon", href: '/favicons/favicon.ico' }],
        ['link', { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/css/uikit.css' }],
        ['script',{ src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js' }],
        ['script',{ src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit-icons.min.js' }],
    ],
    markdown: {
        lineNumbers: false
    },
    themeConfig: {
        logo: '/img/logo.png',
        nav: [
            { text: 'Guide', link: '/guide/' },
            { text: 'Articles', link: '/articles/' },
            { text: 'FAQ', link: '/faq/' },
            { text: 'Forum', link: '/forum/' },
            { text: 'API docs', link: 'https://docs.objectia.com' },
            { text: 'GitHub', link: 'https://github.com/objectia' },
        ],
        sidebar: {
            '/guide/': [{
                title: 'Guide',
                collapsable: false,
                children: [
                    '',
                    ['/guide/gettingstarted', 'Getting Started'],
                ]
            }],
            '/articles/': [{
                title: 'Articles',
                collapsable: false,
                children: [
                    '',
                ]
            }],
            '/faq/': [{
                title: 'Frequently Asked Questions',
                collapsable: false,
            }],
            '/forum/': [{
                title: 'Community Forum',
                collapsable: false,
            }],
        },
    }
}