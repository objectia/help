---
navbar: true
sidebar: false
---

<!-- NO EMPTY LINES BELOW -->
<div class="uk-section uk-section-small uk-text-center">
  <div class="uk-container uk-container-small">
    <h1 style="font-size: 32px">Frequently Asked Questions</h1>
    <p class="uk-text-muted">The most common questions we get from our customers</p>
    <div class="uk-animation-scale-up uk-margin-medium-top">
      <img src="/img/faq/hero.png" style="width:128px">
    </div>
  </div>
</div>
<div class="uk-section uk-section-small">
  <div class="uk-container uk-container-small">
    <h3>Do I need to enter my credit card to sign up?</h3>
    <p>
      No, you don't. As long as you are on the <strong>Basic plan</strong>, we do not need your credit card details. You
      will need to enter your credit card when you upgrade to the <strong>Pro plan</strong>.
    </p>
    <h3>Which plan should I pick?</h3>
    <p>
      We recommend that you start with <strong>Basic plan</strong>, which is free forever. You can upgrade to the
      <strong>Pro plan</strong>, when you need more requests per month.
    </p>
    <h3>Do you have a developer plan?</h3>
    <p>
      No, we don't have a special plan for developers. However, you can use the <strong>Basic plan</strong> where you
      have full access to all features.
    </p>
    <h3>What are the features and limitations of the Basic plan?</h3>
    <p>
      The <strong>Basic plan</strong> is targeted at developers and small businesses. There is a limit on how many
      requests you can send to our servers every day, but you will still get
      the same API response as the <strong>Pro plan</strong>.
    </p>
    <h3>Why only two plans?</h3>
    <p>
      Well, it's very simple indeed. Easy for you and easy for us. You do not have to choose a fixed plan and pay in
      advance. Sometimes it is very difficult to say how big plan you really need.
      It will be a lot of guessing involved. A too small plan will prevent your business to work properly, a too big
      plan and you will waiste money. Hence we have chosen to have one paid plan to
      keep it simple to manage and give all customer fair prices.
    </p>
    <h3>How do you calculate the price of the Pro plan?</h3>
    <p>
      We are using progressive pricing for our services. You start with a resonable price, and the more you are using,
      the less you are paying.
    </p>
    <h3>How can I prevent overspending?</h3>
    <p>
      You can set a spending limit for each service; a maximum number of client requests per month. You can also set up
      alerts so you will be notifed when you are exceeding
      a certain number of requests.
    </p>
    <h3>Can I upgrade/downgrade whenever I want?</h3>
    <p>
      Yes, you can.
    </p>
    <h3>What happens when I downgrade to the Basic plan?</h3>
    <p>
      If you have an outstanding balance, we will charge your credit card. If you are still on the free limit, you pay
      nothing. Upon a successful downgrade, we will remove
      your credit card details from the payment system.
    </p>
    <h3>How are you processing payments?</h3>
    <p>
      We have partnered with <a href="https://www.braintreepayments.com" target="_blank" rel="noopener">Braintree</a>,
      and they handle all our payment processing. Braintree is safely storing
      all your credit card details in their secure vault. We are not able to see or access your credit card details in
      any way.
      <br />
      <img class="uk-margin-top" src="/img/vendor/braintree.png" style="width:100px;opacity: 0.5"
        alt="Braintree payments" />
    </p>
    <h3>What form of payments do you accept?</h3>
    <p>
      We accept Visa, MasterCard, American Express and Discover.
      <br />
      <img class="uk-margin-top" src="/img/vendor/credit-cards.png" alt="credit cards" />
    </p>
    <h3>How am I billed?</h3>
    <p>
      The <strong>Pro plan</strong> is a Pay-as-you-go subscription. We will charge your credit card on the 1st day of
      each month for resources you used the previous month.
    </p>
    <h3>What if you could not process my payment?</h3>
    <p>
      All accounts are due for payment the 1st every month. If we could not charge your card, we will try to process
      your payment two more times before suspending your account.
      We will send you an email if we are unable to process your card. You will also get an email warning you about an
      account suspension.
    </p>
    <h3>What does an account suspension means?</h3>
    <p>
      If your banance from the previous month is not paid, we will flag your account as suspended. This means that your
      API keys will stop working, so all your client requests to our
      servers will fail.
    </p>
    <h3>Oh no, how do I fix a suspension?</h3>
    <p>
      To lift a suspension, go to your account and update your payment card. We will try to charge your card
      immediately. The account will be restored to good standing upon a successful payment.
    </p>
    <h3>Do I get a VAT invoice?</h3>
    <p>
      Yes, you do.
    </p>
    <h3>Where can I find my invoices?</h3>
    <p>
      Invoices are emailed to your account email address. Emails are sent from “<strong>billing@objectia.com</strong>”,
      so please make sure that these emails are not caught in a spam filter.
      You also find all your invoices in the accounts settings.
    </p>
    <h3>Do I have to pay VAT?</h3>
    <p>
      We are a company based in the European Union. If you are registered for VAT in the EU, no VAT will be added to
      your bill. VAT will be added for other customers in the EU.
      Also, VAT will be added for customers from countries outside the EU where it is required.
    </p>
    <h3>Can I sign up on a month-to-month basis?</h3>
    <p>
      All our plans shown are priced on a month-to-month basis.
    </p>
    <h3>Do you have a yearly plan?</h3>
    <p>
      No, we don't.
    </p>
    <h3>How do I cancel my account?</h3>
    <p>
      To cancel your account log in to your account and click the <strong>Close Account</strong> button in settings.
      If you are on the <strong>Pro plan</strong> and have an outstanding balance, we will charge your credit card and
      send you an invoice.
    </p>
    <h3>Do you offer refunds?</h3>
    <p>
      No, we do not offer refunds.
    </p>
    <h3>Do you still keep my details after my account is closed?</h3>
    <p>
      No, we don't. All registered information is deleted right away. The only data we keep are order transactions and
      invoices. We are obliged to
      keep all payment history for up to ten year due to local and EU laws.
    </p>
  </div>
</div>