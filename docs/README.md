---
home: true
heroImage: /img/hero.png
heroText: Objectia Help Center
tagline: How can we help you?
footer: Copyright © 2018-2020 UAB Salesfly. All rights reserved. Objectia is a registered trademark of UAB Salesfly. 
---

<!-- NO EMPTY LINES BELOW -->
<div class="uk-section uk-text-center" style="padding-top: 30px">
  <div class="uk-container">
    <div class="uk-child-width-1-3@l uk-child-width-1-2@m uk-grid-small uk-grid-match" uk-grid
      uk-height-match="target: .uk-card">
      <div>
        <a href="/guide/">
          <div class="uk-card uk-card-default uk-card-body uk-card-hover">
            <h3>Guide</h3>
            <img src="/img/support/guide.png" width="80px" class="uk-animation-scale-up">
            <p>Let us guide you through the system.</p>
          </div>
        </a>
      </div>
      <div>
        <a href="/articles/">
          <div class="uk-card uk-card-default uk-card-body uk-card-hover">
            <h3>Articles</h3>
            <img src="/img/support/articles.png" width="80px" class="uk-animation-scale-up">
            <p>Knowledgebase articles.</p>
          </div>
        </a>
      </div>
      <div>
        <a href="/faq/">
          <div class="uk-card uk-card-default uk-card-body uk-card-hover">
            <h3>FAQ</h3>
            <img src="/img/support/faq.png" width="80px" class="uk-animation-scale-up">
            <p>The most common questions.</p>
          </div>
        </a>
      </div>
      <div>
        <a href="/forum/">
          <div class="uk-card uk-card-default uk-card-body uk-card-hover">
            <h3>Community Forum</h3>
            <img src="/img/support/forum.png" width="80px" class="uk-animation-scale-up">
            <p>Talk to us and your fellow developers in our Slack community.</p>
          </div>
        </a>
      </div>
      <div>
        <a href="https://docs.objectia.com" target="_blank" rel="noopener">
          <div class="uk-card uk-card-default uk-card-body uk-card-hover">
            <h3>API Documentation</h3>
            <img src="/img/support/docs.png" width="80px" class="uk-animation-scale-up">
            <p>Check out the Developer's Guide and the API Reference.</p>
          </div>
        </a>
      </div>
      <div>
        <a href="mailto:support@objectia.com">
          <div class="uk-card uk-card-default uk-card-body uk-card-hover">
            <h3>Contact Us</h3>
            <img src="/img/support/email.png" width="80px" class="uk-animation-scale-up">
            <p>Can't find answer to your question? Contact us by email.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>