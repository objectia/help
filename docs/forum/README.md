---
navbar: true
sidebar: false
---
<!-- NO EMPTY LINES BELOW -->
<div class="uk-section uk-section-small uk-text-center">
      <div class="uk-container uk-container-small">
        <h1 style="font-size: 32px">Objectia community forum</h1>
        <p class="uk-text-muted">Talk with us and your fellow developers on Slack</p>
        <div class="uk-animation-scale-up uk-margin-top">
          <img src="/img/vendor/slack.svg" style="width:200px">
        </div>
        <p>
          <a
            class="uk-button uk-button-primary uk-button-large uk-margin-small-right uk-margin-small-top"
            href="https://objectia-community.slack.com/"
            target="_blank" rel="noopener"
            >Sign in</a
          >
          <a
            class="uk-button uk-button-default uk-button-large uk-margin-small-top"
            href="https://join.slack.com/t/objectia-community/shared_invite/enQtNDEwMTc4NDkyNjc4LTdlNTU4ODY1MzcxYTY3MWMwYjE4NjAwZjE3ZjAzN2JhZmJjZWJjMjhlMDE1MDhmMGVlYWUwNzc2YTJiNWY0ZWI"
            target="_blank" rel="noopener"
            >Join</a
          >
        </p>
    </div>
</div>